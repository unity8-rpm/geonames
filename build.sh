#!/bin/bash


echo "__________________________"
echo "|                        |"
echo "|  SETTING UP THE TREE   |"
echo "|________________________|"

rpmdev-setuptree

echo "_________________________"
echo "|                       |"
echo "|MOVING THE BUILD FILES |"
echo "|_______________________|"

mv *spec ~/rpmbuild/SPECS/
mv source/* ~/rpmbuild/SOURCES/

echo "______________________________"
echo "|                            |"
echo "|INSTALLING THE DEPENDENCIES |"
echo "|____________________________|"

sudo dnf builddep -y ~/rpmbuild/SPECS/*.spec

echo "_________________________"
echo "|                       |"
echo "|  BUILDING THE PACKAGE |"
echo "|_______________________|"

rpmbuild -ba ~/rpmbuild/SPECS/*.spec


echo "____________________________"
echo "|                          |"
echo "|  MOVING THE RPM PACKAGES |"
echo "|__________________________|"

pwd

## this is a temporary solution until I figure out artifacts part
mkdir /builds/unity8-rpm/geonames/artifacts
mv ~/rpmbuild/SRPMS/* /builds/unity8-rpm/geonames/artifacts
mv ~/rpmbuild/RPMS/x86_64/* /builds/unity8-rpm/geonames/artifacts

