Name:		geonames
Version:	r4
Release:	1%{?dist}
Summary:	Parse and query the geonames database dump

Group:		Platform/Tools
License:	LGPL3
URL:		https://github.com/ubports/geonames
Source0:	https://github.com/abhishek9650/geonames/archive/%{name}-%{version}.tar.gz

BuildRequires:	gtk-doc
BuildRequires:  glib2-devel
BuildArch:	x86_64

%description
Parse and query the geonames database dump

%prep
%setup -q
BUILDENV+=('!check')


%build
./autogen.sh  \
	--prefix=/usr
make %{?_smp_mflags}


%check
make check

%install
%make_install

%find_lang %{name}


%files -f %{name}.lang
%{_includedir}/%{name}/geonames.h

%{_libdir}/pkgconfig/geonames.pc
%{_libdir}/libgeonames.so.0.0.0


%changelog
* Sat Sep 29 2018 Abhishek Mudgal <abhishek.mudgal.59@gmail.com>
- First adaptation
